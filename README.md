# WhatShouldWeWatch

Application 100% impartiale pour aider à faire le choix très important de la série à regarder ce soir !

![Capture_d_écran_du_2024-02-26_11-18-08](/uploads/0b3ace47b9bfc93ff9b03ae2b9140d44/Capture_d_écran_du_2024-02-26_11-18-08.png)    
![Capture_d_écran_du_2024-02-26_11-19-42](/uploads/0be1bd381162731f8f5fb566876fe35c/Capture_d_écran_du_2024-02-26_11-19-42.png)

## Technique

Développé en Vue 3 avec Vite et Pinia.

## Project Setup

```sh
npm install
npm run dev
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```
