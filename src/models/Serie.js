export class Serie {

  id;
  name;
  image;
  premiered;
  rating;
  averageRuntime;
  summary;

  wantToWatch;

  constructor (serie = {}) {
    this.id = serie.id;
    this.name = serie.name;
    this.image = serie.image;
    this.premiered = serie.premiered ? new Date(serie.premiered) : undefined;
    this.rating = serie.rating?.average;
    this.averageRuntime = serie.averageRuntime;
    this.summary = serie.summary;
    this.wantToWatch = serie.wantToWatch || false;
  }

}