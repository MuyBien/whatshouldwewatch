import HomeView from "@/views/HomeView.vue";
import MyListView from "@/views/MyListView.vue";
import SearchView from "@/views/SearchView.vue";

import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/search",
      name: "search",
      component: SearchView,
    },
    {
      path: "/my-list",
      name: "MyList",
      component: MyListView,
    },
  ],
});

export default router;