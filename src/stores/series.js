import { Serie } from "@/models/Serie";
import { defineStore } from "pinia";
import { computed, readonly, ref } from "vue";

const useSeriesStore = defineStore("series", () => {

  const series = ref([]);
  series.value = (JSON.parse(localStorage.getItem("series")) || []).map(serie => new Serie(serie));

  /**
   * Séries à regarder
   */
  const seriesToWatch = computed(() => series.value.filter((serie) => serie.wantToWatch));

  /**
   * Permet d'ajouter une série à la liste des séries de l'utilisateur
   * @param {Object} serie
   */
  const addSerie = (serie) => {
    if (series.value.includes(serie)) {
      return;
    }
    series.value.push(serie);
    saveSeries();
  };

  /**
   * Permet de sélectionner ou déselectionner un série
   * @param {Object} serie
   */
  const toggleSelection = (serie) => {
    const index = series.value.findIndex((m) => m.id === serie.id);
    series.value[index].wantToWatch = ! series.value[index].wantToWatch;
    saveSeries();
  };

  /**
   * Sauvegarde les séries dans le localStorage
   */
  const saveSeries = () => {
    localStorage.setItem("series", JSON.stringify(series.value));
  };

  return {
    series: readonly(series),
    seriesToWatch: readonly(seriesToWatch),
    addSerie,
    toggleSelection,
  };
});

export { useSeriesStore };