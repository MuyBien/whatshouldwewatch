import { describe, expect, it } from "vitest";
import { useFilters } from "../useFilters";

const series = [
  {
    id: "1",
    title: "Serie 1",
    averageRuntime: 24,
  },
  {
    id: "2",
    title: "Serie 2",
    averageRuntime: 25,
  },
  {
    id: "3",
    title: "Serie 3",
    averageRuntime: 50,
  },
  {
    id: "4",
    title: "Serie 4",
    averageRuntime: 51,
  },
];

describe("Le composable useFilters", () => {

  it("filtre les séries selon la durée demandée", () => {
    const { filter, selectedFilter, filteredSeries } = useFilters(series);

    expect(filteredSeries.value).toHaveLength(4); // sans filtre

    selectedFilter.value = filter.value.values[0].bounds; // < 25 min
    expect(filteredSeries.value).toHaveLength(1);
    expect(filteredSeries.value[0].id).toBe("1");

    selectedFilter.value = filter.value.values[1].bounds; // 25-50 min
    expect(filteredSeries.value).toHaveLength(2);
    expect(filteredSeries.value[0].id).toBe("2");
    expect(filteredSeries.value[1].id).toBe("3");

    selectedFilter.value = filter.value.values[2].bounds; // > 50 min
    expect(filteredSeries.value).toHaveLength(1);
    expect(filteredSeries.value[0].id).toBe("4");
  });

});