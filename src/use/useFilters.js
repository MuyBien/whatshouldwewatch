import { computed, readonly, ref } from "vue";

const filter = ref({
  name: "Durée des épisodes",
  values: [{
    id: 1,
    label: "Court (< 25 min)",
    bounds: {
      down: 0,
      up: 24,
    },
  }, {
    id: 2,
    label: "Moyen (25-50 min)",
    bounds: {
      down: 25,
      up: 50,
    },
  }, {
    id: 3,
    label: "Long (> 50 min)",
    bounds: {
      down: 51,
      up: Infinity,
    },
  }],
});
const selectedFilter = ref(undefined);

export function useFilters (series) {

  const filteredSeries = computed(() => {
    if (! selectedFilter.value) {
      return series;
    }
    return series.filter((serie) => {
      return serie.averageRuntime <= selectedFilter.value.up && serie.averageRuntime >= selectedFilter.value.down;
    });
  });

  return {
    filter: readonly(filter),
    selectedFilter,
    filteredSeries: readonly(filteredSeries),
  };

}