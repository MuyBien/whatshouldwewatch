import { Serie } from "@/models/Serie";
import { mount } from "@vue/test-utils";
import { beforeEach, describe, expect, it } from "vitest";
import SerieDisplay from "../SerieDisplay.vue";

describe("Le composant d'affichage de série", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(SerieDisplay, { props: { serie: new Serie() } });
  });

  it("affiche le titre des series", async () => {
    await wrapper.setProps({
      serie: new Serie({
        id: 1,
        name: "Film 1",
        image: { medium: "url1" },
      }),
    });
    const listItem = wrapper.find(".serie");
    expect(listItem.text()).toContain("Film 1");
  });

  it("affiche la photo des series", async () => {
    await wrapper.setProps({
      serie: new Serie({
        id: 1,
        name: "Film 1",
        image: { medium: "url1" },
      }),
    });
    const listItem = wrapper.find(".serie");
    expect(listItem.find(".serie-image").attributes("src")).toBe("url1");
  });

  it("affiche un texte si la serie n'a pas d'image", async () => {
    await wrapper.setProps({
      serie: new Serie({
        id: 1,
        name: "Film 1",
        image: null,
      }),
    });
    const listItem = wrapper.find(".serie");
    expect(listItem.find(".no-image").exists()).toBe(true);
  });

  it("affiche la durée moyenne des épisodes", async () => {
    await wrapper.setProps({
      serie: new Serie({
        id: 1,
        name: "Film 1",
        image: null,
        averageRuntime: 60,
      }),
    });
    const listItem = wrapper.find(".serie");
    expect(listItem.text()).toContain("60 min");
  });

  it("affiche la note moyenne", async () => {
    await wrapper.setProps({
      serie: new Serie({
        id: 1,
        name: "Film 1",
        image: null,
        rating: { average: 8.8 },
      }),
    });
    const listItem = wrapper.find(".serie");
    expect(listItem.text()).toContain("8.8");
  });

  it("affiche l'année de première diffusion", async () => {
    await wrapper.setProps({
      serie: new Serie({
        id: 1,
        name: "Film 1",
        image: null,
        premiered: "1993-04-04",
      }),
    });
    const listItem = wrapper.find(".serie");
    expect(listItem.text()).toContain("1993");
  });

});