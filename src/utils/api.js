import { Serie } from "@/models/Serie";

const BASE_URL = "https://api.tvmaze.com";

const fetchSeries = (query) => {
  return fetch(`${BASE_URL}/search/shows?q=${query}`)
    .then(response => response.json())
    .then(data => {
      return data.map(item => new Serie(item.show));
    });
};

export { fetchSeries };